# SFR-Learning

Source code of the SFR-Learning framework and its experimental evaluation for the paper: [Successor Feature Representation](https://arxiv.org/abs/2110.15701).

Authors: [Chris Reinke](https://www.scirei.net/), [Xavier Alameda-Pineda](http://xavirema.eu/)

Copyright: [INRIA](https://www.inria.fr/fr), 2022

License: [GNU General Public License v3.0 or later](https://gitlab.inria.fr/robotlearn/xi_learning/-/blob/master/license.txt)

Contact: [Chris Reinke](chris.reinke@inria.fr)

## Abstract

Transfer in Reinforcement Learning aims to improve learning performance on target tasks using knowledge from experienced source tasks. Successor Representations (SR) and their extension Successor Features (SF) are prominent transfer mechanisms in domains where reward functions change between tasks. They reevaluate the expected return of previously learned policies in a new target task to transfer their knowledge. The SF framework extended SR by linearly decomposing rewards into successor features and a reward weight vector allowing their application in high-dimensional tasks. But this came with the cost of having a linear relationship between reward functions and successor features, limiting its application to such tasks.  We propose a novel formulation of SR based on learning the cumulative discounted probability of successor features, called Successor Feature Representations (SFR). Crucially, SFR allows to reevaluate the expected return of policies for general reward functions. We introduce different SFR variations, prove its convergence, and provide a guarantee on its transfer performance. Experimental evaluations based on SFR with function approximation demonstrate its advantage over SF not only for general reward functions but also in the case of linearly decomposable reward functions.


## Installation

Requirements:
 - Python 3.6 or higher
 - Linux (developed under Ubuntu 18.04) or MacOS

You can either use the installation script to install the software or follow the manual installation steps.

### 1) Installation Script

Use the *install.sh* script to install the software. 

`./install.sh`

### 2) Manual Installation

To run the experiments all python packages under the *src* folder must be installed.
It is recommended to install the package in developer mode (*-e*), so that changes to the source code can be directly used without the need to reinstall the package:

`pip install -e ./src/rlexp`

(If there is a problem with the certifi package, try `pip install -e ./src/rlexp --ignore-installed certifi`)

To be able to load the results with our DataLoader widget in Jupyter notebook, run the following command.
Note: The GUI does not work for Jupyterlab.

`jupyter nbextension enable --py --sys-prefix qgrid`

Optional:
We recommend to install the Jupyter notebook extensions which allows to fold the code and sections.

`pip install jupyter_contrib_nbextensions` \
`jupyter contrib nbextension install --user`


## Overview

The source code has 2 components:

 - rlexp package: The rlexp package contains the code of the agents, environments and the experimental procedure.

 - experiments folder: Contains the parameter settings and the start scripts to run the experiments.
    The experimental results will be stored here and notebooks exist to visualize them.
    Please see the "Usage" section for information about how to run and visualize the experiments.

    Experiments:
      - 01_object_collection_linear_rewards: Object collection environment with linear reward functions
      - 02_object_collection_general_rewards: Object collection environment with general reward functions
      - 03_object_collection_different_non_linearity: Object collection environment with general reward functions of different non-linearity
      - 04_racer: Racer environment
      - 05_barreto_2018_object_collection: Original object collection environment from Barreto et al. (2018)

## Usage

### Running the Experiments

Each experiment has several parameters.
Each parameter defines which algorithm to use and its hyperparameters.
Several repetitions (for the paper n=10) for each parameter are executed which have a different random seed.  
The [exputils](https://gitlab.inria.fr/creinke/exputils) package is used to generate for each parameter and repetition the code from a ODS file and code templates.

Each experiment folder contains an *experiment_configurations.ods*.
This file contains the different parameters, one per row.
Each parameter has an ID (called *Experiment-ID*) which must be unique.
Please note, rows in the *experiment_configurations.ods* that have an empty *Experiment-ID* column are ignored.
In the provided ODS files, only the parameters that resulted in the best performance are currently turned on.
Running all parameters is highly time-consuming!
If you want to run other parameters, copy their IDs from the *Comments* column to the *Experiment ID* column.  

The exputils package uses code templates under the *src* directory to create for each parameter and repetition (number of repetitions are defined in the *repetitions* column of the ODS) the required code.
These are stored in an *experiments* directory which will be automatically generated.

To generate and run the experiments, run the *run_experiments.sh* script:

`./run_experiments.sh`

The command takes as input argument how many experiments (or better repetitions) should run in parallel.
This saves time if you have several CPU's or cores.
Example: `./run_experiments.sh 6`

Please note, you can add new parameters or a higher number of repetitions after your first run in the *experiment_configurations.ods* file.
If you then run the *run_experiments.sh* script, it will only run the repetitions for the new parameters or where more repetitions are required.


#### Learning of features 

Some algorithms use learned features.
The features are learned based on transitions of the initial tasks from the Q-Learning algorithm.
We added to the source code the final learned features, but it is possible to relearn them.
The features can be found in the *learn_features* subdirectory of each experiment.
To restart the learning use the script `./run_feature_training.sh`.

### Analyzing the Results

After the experiments have been finished, the Jupyter notebooks in the *analyze* directory can be used to look at the results.
Each repetition has saved its result in a *data* directory under the *experiments* directory.
The notebooks will load this data and visualize it.

To use the notebooks, first run Jupyter notebook using the following command:

`jupyter notebook`

The *plot_figures.ipynb* replicates the plots that can be found in the paper.

The *overview.ipynb* notebook is used to visualize the data.
The table-widget that is displayed after running the its first cell, allows to load data from specific parameters.
It also allows to set names for each parameter which are then used in the figures.
Please note, loading data from too many parameters might result in an out-of-memory situation!
The other cells can be used to visualize different aspects from the experiments.
