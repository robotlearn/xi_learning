##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import rlexp
import exputils as eu
import exputils.data.logging as log
import torch
import gym
import warnings
from rlexp.agent.project.xi_learning_01 import EnvFeatures

class RewardApproximationGivenRewardFunction:

    @staticmethod
    def default_config():
        return eu.AttrDict()

    def __init__(self, base_class, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.reward_function_components = []
        self.reward_functions = []

    def add_reward_function(self, reward_function, reward_func_descr=None):

        if 'component_per_feature' not in reward_func_descr:
            raise ValueError('Require property \'component_per_feature\' in the reward function description!')

        self.reward_function_components.append(reward_func_descr['component_per_feature'])
        self.reward_functions.append(reward_function)

    def update(self, active_reward_func_idx, transition, exp_info):
        pass

    def get_reward_of_component(self, reward_func_idx, feature_dim, feature_value):
        return self.reward_function_components[reward_func_idx][feature_dim](feature_value)

    def calc_reward(self, feature, reward_func_idx):

        if np.ndim(feature) == 1:
            feature_array = np.array([feature])

        r_func = self.reward_functions[reward_func_idx]
        reward = np.array([r_func(f) for f in feature_array])

        if np.ndim(feature) == 1:
            reward = reward[0]

        return reward


class RewardApproximationLinear:

    @staticmethod
    def default_config():
        return eu.AttrDict(
            alpha_w=0.01,
            w_init_std=0.01,
            absolute_weight_maximum=1000,
            log_reward_approximation_error=False
        )

    def __init__(self, base_class, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.base_class = base_class

        self.w_per_reward_func = []

    def add_reward_function(self, reward_function, reward_func_descr=None):

        w = np.random.randn(self.base_class.n_feature_dim) * self.config.w_init_std
        self.w_per_reward_func.append(w)

    def update(self, active_reward_func_idx, transition, exp_info):

        _, _, reward, _, _, _ = transition
        feature = self.base_class.features.get_feature(transition)

        w = self.w_per_reward_func[active_reward_func_idx]
        r_diff = reward - np.matmul(feature, w)
        w = w + self.config.alpha_w * r_diff * feature
        self.enforce_weight_maximum(w)
        self.w_per_reward_func[active_reward_func_idx] = w

        if self.config.log_reward_approximation_error:
            log.add_value('reward_model_mse_loss', r_diff ** 2)


    def get_reward_of_component(self, reward_func_idx, feature_dim, feature_value):
        return self.w_per_reward_func[reward_func_idx][feature_dim] * feature_value

    def calc_reward(self, feature, reward_func_idx):
        raise NotImplementedError()
        #
        # if np.ndim(feature) == 1:
        #     feature_array = np.array([feature])
        #
        # r_func = self.reward_functions[reward_func_idx]
        # reward = np.array([r_func(f) for f in feature_array])
        #
        # if np.ndim(feature) == 1:
        #     reward = reward[0]
        #
        # return reward

    def enforce_weight_maximum(self, weights):

        if self.config.absolute_weight_maximum is not None:

            weight_above_maximum_inds = weights > self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(self.config.absolute_weight_maximum))

            weight_above_maximum_inds = weights < -1 * self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = -1 * self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(-1 * self.config.absolute_weight_maximum))


class ContinuousFeatureLinearComponentRewardFuncApproxBaseLearnedFeatures:
    """
    Base class for agents that learn an approximation of continuous feature based reward functions that are constructed of a linear combination
    of reward component functions:
        R(phi) = r_1(phi_1) + r_2(phi_2) + r_3(phi_3) + ... + r_n(phi_n)
    """

    @staticmethod
    def default_config():
        return eu.AttrDict(
            is_approximate_reward_function = False,

            features=eu.AttrDict(
                cls=EnvFeatures
            ),

            # if only a alpha_w param is given, then the reward approximation is a simple linear mapping as defined in barreto 2018
            reward_approx = eu.AttrDict(
                alpha_w = 0.01,
                w_init_std = 0.01,
                log_reward_approximation_error=False,
            )

        )


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # obs, feature and action space
        if not isinstance(env.observation_space, gym.spaces.Dict) and 'observation' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'observation\'!')

        self.features = eu.misc.create_object_from_config(self.config.features, env)

        self.observation_space = env.observation_space.spaces['observation']
        self.feature_space = self.features.feature_space
        self.action_space = env.action_space

        if isinstance(self.feature_space, gym.spaces.Box) and len(self.feature_space.shape) == 1:
            # must be a continuous vector
            self.n_feature_dim = self.feature_space.shape[0]
        elif isinstance(self.feature_space, gym.spaces.MultiBinary):
            self.n_feature_dim = self.feature_space.n
        else:
            raise ValueError('Nonsupported feature space type!')

        # identify which reward approximation should be use
        self.reward_approximation_type = ''
        if not self.config.is_approximate_reward_function:
            self.reward_approximation_type = 'none'
        else:
            if 'alpha_w' in self.config.reward_approx:
                self.reward_approximation_type = 'linear'
            if 'model' in self.config.reward_approx:
                if self.reward_approximation_type != '':
                    raise ValueError('The reward_approx configuration has a alpha_w and model attribute, but only one is allowed!')
                self.reward_approximation_type = 'pytorch'

        # configure the reward approximation according to its type
        if self.reward_approximation_type == 'none':
            self.reward_approximation = RewardApproximationGivenRewardFunction(self, config=self.config.reward_approx)
        elif self.reward_approximation_type == 'linear':
            self.reward_approximation = RewardApproximationLinear(self, config=self.config.reward_approx)
        elif self.reward_approximation_type == 'pytorch':
            raise NotImplementedError('PyTorch models are not supported yet!')
        else:
            raise ValueError('Unknown reward approximation type!')

        self.reward_functions = []
        self.active_reward_func_idx = None


    def add_reward_function(self, reward_function, reward_func_descr=None):
        # add reward function

        self.reward_approximation.add_reward_function(reward_function, reward_func_descr)

        self.reward_functions.append(reward_function)

        return len(self.reward_functions) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def get_reward_of_component(self, reward_func_idx, feature_dim, feature_value):
        return self.reward_approximation.get_reward_of_component(reward_func_idx, feature_dim, feature_value)


    def update(self, transition, exp_info):
        """Use the given transition to update the agent"""
        self.reward_approximation.update(self.active_reward_func_idx, transition, exp_info)


    def calc_reward(self, feature, reward_func_idx=None):
        """
        Calculates the reward for a specific feature and reward function.
        """

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        return self.reward_approximation.calc_reward(feature, reward_func_idx)
