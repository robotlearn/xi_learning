##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import gym
import numpy as np
import torch


class ModeledFeatures:

    @staticmethod
    def default_config():
        return eu.AttrDict(
            feature_model_path = None,
        )


    @property
    def feature_space(self):
        return self._feature_space


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if self.config.feature_model_path is None:
            raise ValueError('Path to the feature model must be defined in configuration!')

        # load H from the feature model
        self.feature_model = eu.io.load_dill(self.config.feature_model_path)
        self.n_feature_dim = self.feature_model.n_feature_dim

        if not hasattr(self.feature_model, 'observation_type'):
            self.observation_type = 'state_concatination'
        else:
            self.observation_type = self.feature_model.observation_type

        if self.observation_type == 'state_concatination':
            self._get_obs_func = self._get_obs_concatenated_obs
        elif self.observation_type == 'state_diff':
            self._get_obs_func = self._get_obs_obs_diff
        elif self.observation_type == 'object_memory_diff':
            self._get_obs_func = self._get_obs_memory_diff
        elif self.observation_type == 'feature':
            self._get_obs_func = self._get_obs_feature
        elif self.observation_type == 'pos_and_object_memory_diff':
            self._get_obs_func = self._get_obs_pos_and_object_memory_diff
        else:
            raise ValueError('Unknown observation type!')

        self._feature_space = gym.spaces.Box(low=0.0, high=1.0, shape=(self.n_feature_dim, ), dtype=np.float32)


    def get_feature(self, transition):
        obs = self._get_obs_func(transition)

        # do not compute gradients in case the model is a torch model
        with torch.no_grad():
            feature = self.feature_model(obs)

        return feature


    def _get_obs_concatenated_obs(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        # concatinate obs and next_obs
        obs = obs['observation']
        next_obs = next_obs['observation']
        return np.concatenate((obs, next_obs))


    def _get_obs_obs_diff(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        obs = obs['observation']
        next_obs = next_obs['observation']
        return next_obs - obs


    def _get_obs_memory_diff(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        obs = obs['observation']
        next_obs = next_obs['observation']
        return next_obs[100:112] - obs[100:112]


    def _get_obs_feature(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        return next_obs['feature']


    def _get_obs_pos_and_object_memory_diff(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        return np.concatenate([
            next_obs['observation'][0:100],
            next_obs['observation'][100:112] - obs['observation'][100:112]
        ])