##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import gym


class EnvFeatures:

    @staticmethod
    def default_config():
        return eu.AttrDict()


    @property
    def feature_space(self):
        return self._feature_space


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if not isinstance(env.observation_space, gym.spaces.Dict) and 'feature' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'feature\' attribute!')

        self._feature_space = env.observation_space.spaces['feature']


    def get_feature(self, transition):
        obs, action, reward, next_obs, done, _ = transition
        return next_obs['feature']
