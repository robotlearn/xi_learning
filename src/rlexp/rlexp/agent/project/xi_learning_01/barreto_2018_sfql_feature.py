##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import warnings
import gym
from rlexp.agent.project.xi_learning_01 import EnvFeatures
import exputils.data.logging as log

class Barreto2018SFQLFeature:
    """
    Successor Feature Q-learning agent for continual reward transfer scenarios that uses a single linear layer to
    approximate the Psi-function. Based on SF QL agent in paper [Barreto 2018].
    (https://arxiv.org/pdf/1606.05312.pdf)
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            alpha = 0.05,
            alpha_w = 0.05,
            epsilon = 0.15, # can also be a dict that defines the function to get epsilon
            gamma = 0.95,
            w_init_std = 0.01,
            z_init_std = 0.01,
            z_init = None,
            absolute_weight_maximum = 1000,  # maximum absolute value of weights used for w and z
            is_set_initial_reward_weights=False,
            features = eu.AttrDict(
                cls=EnvFeatures
            ),
            log_reward_model_mse_loss = False
        )
        return dc
        

    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.alpha = self.config.alpha
        self.alpha_w = self.config.alpha_w
        self.epsilon = self.config.epsilon

        self.w_per_reward_func = []
        self.z_per_reward_func = []

        self.active_reward_func_idx = None

        self.c = None

        # obs, feature and action space
        if not isinstance(env.observation_space, gym.spaces.Dict) and 'observation' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'observation\'!')

        self.features = eu.misc.create_object_from_config(self.config.features, env)

        self.observation_space = env.observation_space.spaces['observation']
        self.feature_space = self.features.feature_space
        self.action_space = env.action_space

        if isinstance(self.feature_space, gym.spaces.MultiDiscrete):
            self.feature_len = len(self.feature_space.nvec)
        elif isinstance(self.feature_space, gym.spaces.MultiBinary):
            self.feature_len = self.feature_space.n
        elif isinstance(self.feature_space, gym.spaces.Box) and len(self.feature_space.shape) == 1:
            self.feature_len = self.feature_space.shape[0]
        else:
            raise ValueError('Nonsupported feature_space object!')


    def add_reward_function(self, reward_function, reward_func_descr=None):

        if self.config.is_set_initial_reward_weights:
            # use given reward weight vector
            w = reward_func_descr['reward_weight_vector'].copy()
        else:
            # set up initial weight vector for the new reward function
            # line 2
            w = np.random.randn(self.feature_len) * self.config.w_init_std
        self.w_per_reward_func.append(w)

        # set up initial psi-function for the new reward function
        # line 3
        if not self.z_per_reward_func:
            if self.config.z_init is None:
                z = np.random.randn(self.action_space.n, self.observation_space.shape[0], self.feature_len) * self.config.z_init_std
            else:
                z = self.config.z_init.copy()
        else:
            z = self.z_per_reward_func[self.active_reward_func_idx].copy()
        self.z_per_reward_func.append(z)

        return len(self.z_per_reward_func) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def get_action(self, obs, info, exp_info):
        """Draws an epsilon-greedy policy"""

        obs = obs['observation']

        # line 10 - 14
        action, self.c = self.calc_max_action(obs)
        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent."""

        # extract transition information
        obs, action, reward, next_obs, done, _ = transition
        feature = self.features.get_feature(transition)
        obs = obs['observation']
        next_obs = next_obs['observation']

        # get policy c from which the current gpi action was taken
        # line 10
        c = self.c

        # lines 15 - 20
        if done:
            gamma = 0
            next_action = 0  # just a dummy action
        else:
            gamma = self.config.gamma
            next_action, _ = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)

        # update the reward weights
        # line 21
        w = self.w_per_reward_func[self.active_reward_func_idx]
        r_diff = reward - np.matmul(feature, w)
        w = w + self.alpha_w * r_diff * feature
        self.enforce_weight_maximum(w)
        self.w_per_reward_func[self.active_reward_func_idx] = w

        if self.config.log_reward_model_mse_loss:
            log.add_value('reward_model_mse_loss', r_diff**2)

        # update current psi function
        # lines 22 - 23
        z = self.z_per_reward_func[self.active_reward_func_idx]
        current_psi = np.matmul(obs, z[action, :])
        next_psi = np.matmul(next_obs, z[next_action, :])
        for k in range(self.feature_len):
            z[action, :, k] = z[action, :, k] + self.alpha * (feature[k] + gamma * next_psi[k] - current_psi[k]) * obs
        self.enforce_weight_maximum(z)

        # update previous policy, if it provided the gpi action
        # lines 25 - 30
        if c != self.active_reward_func_idx:
            # line 26
            next_action, _ = self.calc_max_action(next_obs, policy_idx=c, target_reward_func_idx=c)

            # lines 27 - 28
            z = self.z_per_reward_func[c]
            current_psi = np.matmul(obs, z[action, :])
            next_psi = np.matmul(next_obs, z[next_action, :])
            for k in range(self.feature_len):
                z[action, :, k] = z[action, :, k] + self.alpha * (feature[k] + gamma * next_psi[k] - current_psi[k]) * obs
            self.enforce_weight_maximum(z)


    def calc_max_action(self, state, policy_idx=None, target_reward_func_idx=None):
        """
        Get the optimal action for a given reward function.
        If policy_idx is None, then it gets the GPI optimal action
        """

        if policy_idx is None:
            policy_idx = list(range(len(self.z_per_reward_func)))
        elif not isinstance(policy_idx, list):
            policy_idx = [policy_idx]

        if target_reward_func_idx is None:
            target_reward_func_idx = self.active_reward_func_idx

        # calculate the Q-values for all actions
        # Q = phi * z * w
        q_values = np.zeros((len(policy_idx), self.action_space.n))
        for idx in range(len(policy_idx)):
            psi = np.matmul(state, self.z_per_reward_func[policy_idx[idx]])
            q_values[idx, :] = np.matmul(psi, self.w_per_reward_func[target_reward_func_idx])

        # identify the optimal policy and action
        # select action with highest return over all policies
        max_value = np.max(q_values)
        where_max_value = np.where(q_values == max_value)
        n_max_values = len(where_max_value[0])
        if n_max_values == 1:
            selected_val_idx = 0
        else:
            selected_val_idx = np.random.randint(n_max_values)
        max_policy_idx = where_max_value[0][selected_val_idx]
        max_action = where_max_value[1][selected_val_idx]

        return max_action, policy_idx[max_policy_idx]


    def calc_psi_function(self, state, action, reward_function_idx=None):

        if reward_function_idx is None:
            reward_function_idx = self.active_reward_func_idx

        psi = np.matmul(state, self.z_per_reward_func[reward_function_idx][action, :, :])
        return psi


    def calc_expected_return(self, state, action=None, policy_idx='all', reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if np.ndim(state) == 1:
            states = [state]
        else:
            states = state

        if action is not None:
            check_actions = [action]
        else:
            check_actions = [a for a in range(self.action_space.n)]

        if policy_idx == 'all':
            policy_idxs = [i for i in range(len(self.z_per_reward_func))]
        else:
            policy_idxs = [policy_idx]

        q_values = np.full((np.shape(states)[0], len(policy_idxs), len(check_actions)), np.nan)
        for q_values_p_idx, p_idx in enumerate(policy_idxs):
            for a_idx, a in enumerate(check_actions):
                psi = np.matmul(state, self.z_per_reward_func[p_idx][a, :, :])
                q_values[:, q_values_p_idx, a_idx] = np.matmul(psi, self.w_per_reward_func[reward_func_idx])

        q_max = np.max(q_values, axis=1)
        if np.ndim(state) == 1:
            ret_val = q_max[0]
        else:
            ret_val = q_max

        if action is not None:
            ret_val = ret_val[0]

        return ret_val


    def enforce_weight_maximum(self, weights):

        if self.config.absolute_weight_maximum is not None:

            weight_above_maximum_inds = weights > self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(self.config.absolute_weight_maximum))

            weight_above_maximum_inds = weights < -1 * self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = -1 * self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(-1 * self.config.absolute_weight_maximum))
