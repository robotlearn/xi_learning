##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
from rlexp.agent.project.xi_learning_01.env_features import EnvFeatures
from rlexp.agent.project.xi_learning_01.modeled_features import ModeledFeatures
from rlexp.agent.project.xi_learning_01.discrete_feature_reward_func_approx_base import DiscreteFeatureRewardFuncApproxBase

from rlexp.agent.project.xi_learning_01.dnn_q_learning import DNNQLearning
from rlexp.agent.project.xi_learning_01.barreto_2018_sfql_pytorch import Barreto2018SFQLPyTorch
from rlexp.agent.project.xi_learning_01.barreto_2018_sfql_pytorch_feature import Barreto2018SFQLPyTorchFeature

from rlexp.agent.project.xi_learning_01.q_learning_numpy import QLearningNumpy
from rlexp.agent.project.xi_learning_01.discrete_mf_xi import DiscreteMFXi
from rlexp.agent.project.xi_learning_01.discrete_mb_xi import DiscreteMBXi
from rlexp.agent.project.xi_learning_01.barreto_2018_sfql import Barreto2018SFQL
from rlexp.agent.project.xi_learning_01.barreto_2018_sfql_feature import Barreto2018SFQLFeature

from rlexp.agent.project.continuous_sf_01.continuous_feature_reward_linear_component_func_approx_base import ContinuousFeatureLinearComponentRewardFuncApproxBase
from rlexp.agent.project.continuous_sf_01.continuous_feature_reward_linear_component_func_approx_base_learned_features import ContinuousFeatureLinearComponentRewardFuncApproxBaseLearnedFeatures

from rlexp.agent.project.continuous_sf_01.oned_discretized_continuous_phi_mf_xi import OneDDiscretizedContinuousPhiMFXi
from rlexp.agent.project.continuous_sf_01.oned_discretized_continuous_phi_mf_xi_feature import OneDDiscretizedContinuousPhiMFXiFeature

from rlexp.agent.project.continuous_sf_01.oned_discretized_continuous_phi_mf_xi_feature_numpy import OneDDiscretizedContinuousPhiMFXiFeatureNumpy

