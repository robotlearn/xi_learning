##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import rlexp
import exputils as eu
import exputils.data.logging as log
import torch
import gym


class DiscreteFeatureRewardFuncApproxBase:
    """
    Base class for agents that learn an approximation of discrete feature based reward functions.
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            is_approximate_reward_function = False,

            log_reward_approximation_error = False,

            reward_approx = eu.AttrDict(
                model=eu.AttrDict(
                    cls=rlexp.approximation.LinearNN,
                ),

                optimizer=eu.AttrDict(
                    cls=torch.optim.SGD,
                    lr=0.01,
                ),

                loss=eu.AttrDict(
                    cls=torch.nn.MSELoss,
                ),

                replay_buffer=eu.AttrDict(
                    cls=rlexp.approximation.ReplayBuffer,
                    capacity=1,
                    batch_size=1,
                ),

                init_mode='random',  # 'random' or 'previous'

                n_iterations_per_training = 1,

                gradient_clipping_value=1.0,
            ),

        )
        return dc


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # obs, feature and action space
        if not isinstance(env.observation_space, gym.spaces.Dict) and 'observation' in env.observation_space.spaces and 'feature' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'observation\' and \'feature\' attribute!')

        self.observation_space = env.observation_space.spaces['observation']
        self.feature_space = env.observation_space.spaces['feature']
        self.action_space = env.action_space

        if isinstance(self.feature_space, gym.spaces.MultiDiscrete):
            self.n_feature_values = self.feature_space.nvec
            self.n_feature_dim = len(self.feature_space.nvec)
        elif isinstance(self.feature_space, gym.spaces.MultiBinary):
            self.n_feature_values = np.array([2] * self.feature_space.n)
            self.n_feature_dim = self.feature_space.n
        else:
            raise ValueError('Nonsupported feature_space object!')
        self.n_feature_combinations = np.prod(self.n_feature_values)

        # create all possible combinations of features
        feature_values_lists = [np.arange(n) for n in self.n_feature_values]
        self.all_feature_combs = torch.tensor(
            np.transpose(
                np.meshgrid(*feature_values_lists, indexing='ij'),
                np.roll(np.arange(len(feature_values_lists) + 1), -1)
            ).reshape(-1, len(feature_values_lists))
        )

        self.reward_functions = []
        self.active_reward_func_idx = None
        self._rewards_of_all_feature_combs = []


    def add_reward_function(self, reward_function, reward_func_descr=None):

        ########
        # add reward function

        if self.config.is_approximate_reward_function:
            new_r_func_approx = eu.AttrDict()

            new_r_func_approx.model = eu.misc.call_function_from_config(
                self.config.reward_approx.model,
                self.n_feature_dim,     # input dimension
                1,                      # output dimensions
                func_attribute_name='cls'
            )

            # copy the parameters of the previous reward function
            if self.config.reward_approx.init_mode == 'previous' and self.reward_functions:
                new_r_func_approx.model.load_state_dict(
                    self.reward_functions[self.active_reward_func_idx].model.state_dict())

            new_r_func_approx.loss_func = eu.misc.call_function_from_config(
                self.config.reward_approx.loss,
                func_attribute_name='cls'
            )

            new_r_func_approx.optimizer = eu.misc.call_function_from_config(
                self.config.reward_approx.optimizer,
                new_r_func_approx.model.parameters(),
                func_attribute_name='cls'
            )

            new_r_func_approx.replay_buffer = eu.misc.call_function_from_config(
                self.config.reward_approx.replay_buffer,
                func_attribute_name='cls'
            )

            self.reward_functions.append(new_r_func_approx)

        else:
            self.reward_functions.append(reward_function)

            # calculate reward for each combination: rewards = [comb_idx]
            self._rewards_of_all_feature_combs.append(torch.tensor(
                [reward_function(comb) for comb in self.all_feature_combs],
                dtype=torch.float32
            ))

        # return new rfunc index
        return len(self.reward_functions) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def calc_rewards_of_all_feature_combinations(self, reward_func_idx):

        if self.config.is_approximate_reward_function:
            reward_func_approx_model = self.reward_functions[reward_func_idx].model

            all_feature_combs = self.all_feature_combs.to(reward_func_approx_model.dtype)
            rewards = reward_func_approx_model(all_feature_combs).squeeze()

        else:
            # calculate reward for each combination: rewards = [comb_idx]
            # the final feature gives zero reward as it stands for steps after the goal stae was reached
            rfunc = self.reward_functions[reward_func_idx]
            rewards = torch.tensor(
                [rfunc(comb) for comb in self.all_feature_combs],
                dtype=torch.float32
            )

        return rewards


    def get_rewards_of_all_feature_combs_as_tensor(self, reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        # get the rewards of all possible feature combinations
        if self.config.is_approximate_reward_function:
            reward_func_approx_model = self.reward_functions[reward_func_idx].model
            all_feature_combs = self.all_feature_combs.to(reward_func_approx_model.dtype)
            rewards = reward_func_approx_model(all_feature_combs).squeeze()
        else:
            rewards = self._rewards_of_all_feature_combs[reward_func_idx]

        return rewards


    def get_rewards_of_all_feature_combs(self, reward_func_idx=None):
        r = self.get_rewards_of_all_feature_combs_as_tensor(reward_func_idx=reward_func_idx)
        return r.detach().numpy()


    def update(self, transition, exp_info):
        """Use the given transition to update the agent"""

        _, _, reward, next_obs, _, _ = transition
        phi = next_obs['feature']

        ##############
        # add transition data to reward function approximator if needed
        if self.config.is_approximate_reward_function:
            active_reward_function_approx = self.reward_functions[self.active_reward_func_idx]

            # add transition to replay buffer for active reward function
            data = (torch.tensor(phi, dtype=active_reward_function_approx.model.dtype),
                    torch.tensor([reward], dtype=active_reward_function_approx.model.dtype))

            active_reward_function_approx.replay_buffer.add(data)

        #############
        # train the models
        self.train_reward_func_approx()


    def train_reward_func_approx(self, n_iterations=None):
        """
        Trains the reward function approximator for n_iterations (either specified as attribute or in config) from samples
        """

        #########
        # train reward function if needed
        if self.config.is_approximate_reward_function:

            active_reward_function_approx = self.reward_functions[self.active_reward_func_idx]

            if len(active_reward_function_approx.replay_buffer) >= active_reward_function_approx.replay_buffer.config.batch_size:

                mean_loss = 0.0

                for iter in range(self.config.reward_approx.n_iterations_per_training):

                    # sample batch
                    phi_batch, reward_batch = active_reward_function_approx.replay_buffer.sample_and_split_as_tensors()

                    approx_reward_batch = active_reward_function_approx.model(phi_batch)

                    # calc loss
                    loss = active_reward_function_approx.loss_func(approx_reward_batch, reward_batch)

                    # backward pass
                    active_reward_function_approx.optimizer.zero_grad()
                    loss.backward()

                    # avoid exploding weights and gradients using gradient clipping
                    if self.config.reward_approx.gradient_clipping_value is not None:
                        torch.nn.utils.clip_grad_value_(active_reward_function_approx.model.parameters(),
                                                        clip_value=self.config.reward_approx.gradient_clipping_value)

                    # optimize the model
                    active_reward_function_approx.optimizer.step()

                    mean_loss += loss.item()

            if self.config.log_reward_approximation_error:
                mean_loss = mean_loss / self.config.reward_approx.n_iterations_per_training
                log.add_value('reward_approximation_error_per_step', mean_loss)


    def calc_reward(self, feature, reward_func_idx=None):
        """
        Calculates the reward for a specific feature and reward function.
        """

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if self.config.is_approximate_reward_function:
            reward_func_approx = self.reward_functions[reward_func_idx]

            if np.ndim(feature) == 1:
                feature_tensor = torch.tensor([feature], dtype=reward_func_approx.model.dtype)
            else:
                feature_tensor = torch.tensor(feature, dtype=reward_func_approx.model.dtype)

            with torch.no_grad():
                reward_tensor = reward_func_approx.model(feature_tensor)

            reward = reward_tensor.numpy()

        else:

            if np.ndim(feature) == 1:
                feature_array = np.array([feature])

            r_func = self.reward_functions[reward_func_idx]
            reward = np.array([r_func(f) for f in feature_array])

        if np.ndim(feature) == 1:
            reward = reward[0]

        return reward


