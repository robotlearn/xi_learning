##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import gym
import exputils as eu
from rlexp.env.project.xi_learning_01.twod_object_collection import TwoDObjectCollection


class Barreto2018TwoDObjectCollection(TwoDObjectCollection):
    """
    2D grid object collection task described in Barreto et al. (2018) (https://arxiv.org/pdf/1606.05312.pdf).
    """

    @staticmethod
    def default_config():
        dc = TwoDObjectCollection.default_config()

        dc.observation_space = eu.AttrDict()

        # create list of all components for the gaussian map
        dim1_components = np.linspace(dc.area[0], dc.area[2], 10)
        dim2_components = np.linspace(dc.area[1], dc.area[3], 10)
        dc.observation_space.position_components = np.array(np.meshgrid(dim1_components, dim2_components)).T.reshape(-1, 2)

        dc.observation_space.position_components_sigma = 0.1

        return dc


    def __init__(self, config=None, **kwargs):

        super(Barreto2018TwoDObjectCollection, self).__init__(config=config, **kwargs)

        self.observation_space = gym.spaces.Dict(
            observation=gym.spaces.Box(
                low=-np.inf,
                high=np.inf,
                shape=(self.config.observation_space.position_components.shape[0] + self.config.objects.shape[0] + 1, )
            ),
            feature=self.observation_space['feature']
        )


    def get_obs(self, agent_pos=None, exist_object=None):
        """
        Returns the associated observation for the internal state defined by the x,y position of the agent and if certain objects still exists.

        :param agent_pos: numpy array with [x, y] position of the agent
        :param exist_object: boolean numpy array with objects that still exist in the environment.
        :return: observation as numpy array
        """

        obs = super(Barreto2018TwoDObjectCollection, self).get_obs()

        if agent_pos is None:
            agent_pos = self.state_agent_pos

        if exist_object is None:
            exist_object = self.state_exist_object

        # compute gaussian map
        phi_p = np.exp(-1 * np.sum((agent_pos - self.config.observation_space.position_components)**2, axis=1) / self.config.observation_space.position_components_sigma)

        # get objects that have been picked up
        phi_i = np.logical_not(exist_object)

        # concatenate both with a an constant term
        phi = np.concatenate((phi_p, phi_i, [1.0]))

        obs['observation'] = phi

        return obs


def run_env_test():

    env = Barreto2018TwoDObjectCollection()

    env.reset()
    env.render()

    for step_idx in range(10):
        action = env.action_space.sample()
        obs = env.step(action)
        env.render()

        print(obs)


if __name__ == '__main__':
    run_env_test()
