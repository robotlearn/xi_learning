##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import gym
import exputils as eu
import rlexp
from rlexp.env.project.continuous_sf_01 import TwoDInfiniteTrackRacerEnv


class RBFStateTwoDInfiniteTrackRacerEnv(TwoDInfiniteTrackRacerEnv):
    """

    """

    @staticmethod
    def default_config():
        dc = TwoDInfiniteTrackRacerEnv.default_config()

        dc.observation_space = eu.AttrDict()

        # create list of all components for the gaussian map
        dim1_components = np.linspace(dc.area[0][0], dc.area[0][1], 10)
        dim2_components = np.linspace(dc.area[1][0], dc.area[1][1], 10)
        dc.observation_space.position_components = np.array(np.meshgrid(dim1_components, dim2_components)).T.reshape(-1, 2)
        dc.observation_space.position_components_sigma = 0.1

        dc.observation_space.direction_components = np.linspace(-np.pi, np.pi, num=20, endpoint=False)
        dc.observation_space.direction_components_sigma = np.pi*0.2

        return dc


    def __init__(self, config=None, **kwargs):

        super(RBFStateTwoDInfiniteTrackRacerEnv, self).__init__(config=config, **kwargs)

        self.observation_space = gym.spaces.Dict(
            observation=gym.spaces.Box(
                low=-np.inf,
                high=np.inf,
                shape=(self.config.observation_space.position_components.shape[0] + self.config.observation_space.direction_components.shape[0] + 1,)
            ),
            feature=self.observation_space['feature']
        )


    def get_obs(self, agent_state=None):
        """
        Returns the associated observation for the internal state defined by the x,y position of the agent and if certain objects still exists.

        :param agent_state: numpy array with [x, y, angle] of the agent
        :return: observation as numpy array
        """

        obs = super(RBFStateTwoDInfiniteTrackRacerEnv, self).get_obs(agent_state=agent_state)

        if agent_state is None:
            agent_pos = self.state_pos
            agent_angle = self.state_angle
        else:
            agent_pos = agent_state[:2]
            agent_angle = agent_state[2]

        # compute gaussian map
        diff_x, diff_y = self.calc_min_difference(agent_pos, self.config.observation_space.position_components)

        rbf_pos = np.exp(-1 * (diff_x**2 + diff_y**2) / self.config.observation_space.position_components_sigma)

        # get objects that have been picked up

        angle_distance = np.abs(agent_angle - self.config.observation_space.direction_components)
        angle_distance = np.minimum(angle_distance, 2*np.pi - angle_distance)

        rbf_angle = np.exp(-1 * angle_distance**2 / self.config.observation_space.position_components_sigma)

        # concatenate both with a an constant term
        rbf_state = np.concatenate((rbf_pos, rbf_angle, [1.0]))

        obs['observation'] = rbf_state

        return obs


if __name__ == '__main__':

    env = RBFStateTwoDInfiniteTrackRacerEnv()

    env.reward_function, _, _ = rlexp.env.project.continuous_sf_01.twod_infinite_track_racer_env.sample_component_independent_rfunc(env.config.markers.shape[0])

    env.reset()
    env.render()

    for step_idx in range(20):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        env.render()