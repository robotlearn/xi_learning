##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import rlexp.agent
import rlexp.env
import rlexp.exp
import rlexp.utils
import rlexp.approximation
from rlexp.exp.core import run_training


__version__ = '0.0.1'