#!/usr/bin/env python
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import rlexp
from repetition_config import config

def log_transitions(log,env,agent,step,episode,step_per_episode,transition,exp_info):

    # only record transitions from the QL agents to be used to learn the features
    if isinstance(agent, (rlexp.agent.project.xi_learning_01.QLearningNumpy, rlexp.agent.project.xi_learning_01.DNNQLearning)):

        # only record the first 20 tasks (i.e. phases)
        if exp_info.phase < 20:

            log.add_value('transitions', transition)
            log.add_value('phase_per_transition', exp_info.phase)


# run experiment resuming from a potential previous run
rlexp.exp.continual_rf_transfer.run_training(
    resume_from_log=True,
    save_log_automatically=True,
    config=config,
    log_functions=log_transitions
)