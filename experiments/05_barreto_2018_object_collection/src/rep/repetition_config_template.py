##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import rlexp
import rlexp.agent.project.xi_learning_01 as agents
import rlexp.utils as utils
import gym
import torch

config = eu.AttrDict(
    seed = 4568745 + <repetition_id>,

    agent = eu.AttrDict(
        cls=agents.<agent>,
        features=eu.AttrDict(
            cls = agents.<feature_type>
        )
    ),

    env = eu.AttrDict(
        cls=rlexp.env.project.xi_learning_01.Barreto2018TwoDObjectCollection
    ),

    n_tasks = <n_tasks>,
    n_max_steps_per_phase = 20000,
)

################
# agent configuration depends on the used agent (config.agent_class)

# set general agent config
config.agent = eu.combine_dicts(config.agent, dict(<agent_config>), config.agent.cls.default_config())

if config.agent.cls == agents.QLearningNumpy:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <q_lr>

if config.agent.cls == agents.Barreto2018SFQL:
    config.agent.epsilon = <epsilon>
    config.agent.alpha_w = <r_lr>
    config.agent.alpha = <psi_lr>

if config.agent.cls == agents.Barreto2018SFQLFeature:
    config.agent.epsilon = <epsilon>
    config.agent.alpha_w = <r_lr>
    config.agent.alpha = <psi_lr>

if config.agent.cls == agents.DiscreteMFXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

if config.agent.cls == agents.DiscreteMBXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.onestep_sf_model = eu.combine_dicts(dict(<sf_model_config>), config.agent.onestep_sf_model)
    config.agent.onestep_sf_model.optimizer.lr = <sf_model_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

if config.agent.cls == agents.OneDDiscretizedContinuousPhiMFXiFeature:
    config.agent.xi_func = eu.combine_dicts(dict(<xi_config>), config.agent.xi_func)
    config.agent.xi_func.optimizer.lr = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.alpha_w = <r_lr>

if config.agent.cls == agents.OneDDiscretizedContinuousPhiMFXiFeatureNumpy:
    config.agent.xi_func = eu.combine_dicts(dict(<xi_config>), config.agent.xi_func)
    config.agent.xi_func.alpha = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.alpha_w = <r_lr>

# provide the path to the feature model if one should be used
# this depends on the number of features <n_feature_dims> which defines experiment id used for computing the features
if '<feature_type>' == 'ModeledFeatures':
    # the learned features have experiment ids with 4xx and 8xx where x is the same repetition as the current one
    src_feature_exp_id = <n_feature_dims> * 100 + <repetition_id>
    config.agent.features.feature_model_path = '../../../learn_features/experiments/{}/{}/data/final_model.dill'.format(
        eu.EXPERIMENT_DIRECTORY_TEMPLATE.format(src_feature_exp_id),
        eu.REPETITION_DIRECTORY_TEMPLATE.format(0), # always use repetition 0 from learned features
    )


##################
# sample random tasks
env = eu.misc.create_object_from_config(config.env)
feature_dim = env.observation_space['feature'].n

# set seed so that the sampling of the tasks is deterministic
eu.misc.seed(config)
# sample weights between [-1, 1] for features for objects
reward_weights_per_task = np.random.rand(config.n_tasks, feature_dim - 1) * 2 - 1
# add weight of 1 for the feature of ending in the goal state
reward_weights_per_task = np.hstack((reward_weights_per_task, np.ones((config.n_tasks, 1))))

reward_functions = []
for task_idx in range(config.n_tasks):
    reward_weights = reward_weights_per_task[task_idx, :]
    reward_func = lambda feature, w=reward_weights: np.sum(w * np.array(feature))
    reward_func_descr = dict(reward_weight_vector=reward_weights)
    reward_functions.append((reward_func, reward_func_descr))
config.reward_functions = reward_functions

# remove the first 20 tasks if learned features are used
# as these tasks were used to collect the data to learn the features
if '<feature_type>' in ['ModeledFeatures', 'LinearFeatures']:
    config.reward_functions = config.reward_functions[20:]