##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import numpy as np
import rlexp
import matplotlib.pyplot as plt

def sample_features(env, reward_function, batch_size, x_pos_range, y_pos_range):

    random_positions = np.random.rand(batch_size, 2)

    x_pos = x_pos_range[0] + (x_pos_range[1] - x_pos_range[0]) * random_positions[:, 0]
    y_pos = y_pos_range[0] + (y_pos_range[1] - y_pos_range[0]) * random_positions[:, 1]

    features = []
    rewards = np.empty(batch_size)

    for sample_idx in range(batch_size):

        obs = env.get_obs(np.array([x_pos[sample_idx], y_pos[sample_idx], 0.0]))
        feature = obs['feature']

        features.append(feature)
        rewards[sample_idx] = reward_function(feature)

    features = np.array(features)

    return features, rewards


# def sample_features(env, reward_function, batch_size, x_pos_range, y_pos_range):
#
#     random_positions = np.random.rand(batch_size, 2)
#
#     x_positions = np.linspace(x_pos_range[0], x_pos_range[1], batch_size)
#     y_positions = np.linspace(x_pos_range[0], x_pos_range[1], batch_size)
#
#     features = []
#     rewards = []
#
#     for x_pos in x_positions:
#         for y_pos in y_positions:
#
#             obs = env.get_obs(np.array([x_pos, y_pos, 0.0]))
#             feature = obs['feature']
#
#             features.append(feature)
#             rewards.append(reward_function(feature))
#
#     features = np.array(features)
#     rewards = np.array(rewards)
#
#     return features, rewards


# def sample_features(env, reward_function, batch_size, x_pos_range, y_pos_range):
#
#     features = []
#     rewards = []
#
#     env.reset()
#     for step_idx in range(batch_size):
#         action = env.action_space.sample()
#         obs, _, _, _ = env.step(action)
#         feature = obs['feature']
#
#         features.append(feature)
#         rewards.append(reward_function(feature))
#
#     features = np.array(features)
#     rewards = np.array(rewards)
#
#     return features, rewards


def train_linear_model(reward_function, env, config=None, **kwargs):

    default_config = eu.AttrDict(

        # environment for which the weights should be trained
        n_iterations = 100,

        batch_size = 10,

        learn_rate = 1.0,

        x_pos_range = [0.0, 1.0],
        y_pos_range = [0.0, 1.0],

        reduction = 'mean', # 'mean' or 'sum'
    )

    config = eu.combine_dicts(kwargs, config, default_config)

    if config.reduction not in ['mean', 'sum']:
        raise ValueError('Unknown value for reduction configuration: {!r}! Allowed values: \'mean\', \'sum\'.'.format(config.reduction))

    if config.reduction == 'mean':
        reduction_func = np.mean
    else:
        reduction_func = np.sum

    if isinstance(env, dict):
        env = eu.misc.create_object_from_config(env)

    log_mean_error = np.empty(config.n_iterations)

    # random initialization of weight
    w = np.random.randn(env.observation_space['feature'].shape[0]) * 0.01

    for iter in range(config.n_iterations):

        features, reward_per_feature = sample_features(env, reward_function, config.batch_size, config.x_pos_range, config.y_pos_range)

        expected_reward_per_feature = np.matmul(features, w)

        diff_per_feature = reward_per_feature - expected_reward_per_feature

        delta_w_per_feature = np.transpose([diff_per_feature]) * features

        reduced_delta_w = reduction_func(delta_w_per_feature, axis=0)

        w += config.learn_rate * reduced_delta_w

        # logging
        log_mean_error[iter] = np.mean(np.abs(diff_per_feature))

    return w, log_mean_error


def sample_component_independent_rfunc(n_markers,
                                 n_possible_gauss_components=None,
                                 mu_range=None,
                                 sigma_range=None,
                                 weight_range=None,
                                 wall_distance_feature=None):
    if n_possible_gauss_components is None:
        n_possible_gauss_components = [1, 2, 3]
    if mu_range is None:
        mu_range = [0.0, 1.0]
    if sigma_range is None:
        sigma_range = [0.001, 0.01]
    if weight_range is None:
        weight_range = [-1.0, 1.0]

    def sample_range(range_def):
        return range_def[0] + (range_def[1] - range_def[0]) * np.random.rand()

    gauss_template = 'np.exp(-(<VALUE> - {:.3f})**2 / {:.5f})'  # placeholders: mu, sigma

    component_functions = []
    component_strings = []

    # marker distance features
    for feature_dim in range(n_markers):

        n_gauss_components = np.random.choice(n_possible_gauss_components)

        weight = sample_range(weight_range) / n_markers

        component_str = '{:.3f} * np.max(['.format(weight)

        for gauss_component_idx in range(n_gauss_components):

            component_str += gauss_template.format(
                sample_range(mu_range),
                sample_range(sigma_range)
            )

            if gauss_component_idx < n_gauss_components - 1:
                component_str += ', '

        component_str += '])'

        component_functions.append(eval('lambda feature_val: ' + component_str.replace('<VALUE>', 'feature_val')))
        component_strings.append(component_str.replace('<VALUE>', 'features[{}]'.format(feature_dim)))

    # wall distance feature
    if wall_distance_feature is not None:
        wall_distance_weight = wall_distance_feature[0]
        wall_distance_sigma = wall_distance_feature[1]

        component_str = '{} * '.format(wall_distance_weight) + gauss_template.format(
            0.0,
            wall_distance_sigma
        )

        component_functions.append(eval('lambda feature_val: ' + component_str.replace('<VALUE>', 'feature_val')))
        component_strings.append(component_str.replace('<VALUE>', 'features[-1]'))

    # create final reward function from components
    lambda_str = 'lambda features: ' + ' + '.join(component_strings)
    lambda_func = eval(lambda_str)

    return lambda_func, lambda_str, component_functions


def eval_train_linear_model_on_non_linear_reward_function():

    n_tasks = 1

    env_config = eu.AttrDict(
        cls = rlexp.env.project.continuous_sf_01.RBFStateTwoDTrackRacerEnv,
        start_state=[
            [0.1, 0.9],  # x
            [0.1, 0.9],  # y
            [-np.pi, np.pi]  # angle
        ],
        stochasticity = (0.005, 0.005, 0.005),
        markers=np.array([
            [0.25, 0.75],
            [0.75, 0.25],
            [0.75, 0.6],
        ]),
        wall_proximity_feature = True,
    )

    # set seed so that the sampling of the tasks is deterministic
    eu.misc.seed(5)

    # random generator of weights for each feature
    # get number of markers in env
    env = eu.misc.create_object_from_config(env_config)
    n_markers_in_env = env.config.markers.shape[0]

    non_linear_reward_functions = []
    for task_idx in range(n_tasks):
        rf_handle, rf_str, rf_component_handles = sample_component_independent_rfunc(
            n_markers_in_env,
            mu_range=[0.0, 0.7],
            weight_range=[-0.5, 1.0],
            wall_distance_feature=(-1.0, 0.001)
        )
        non_linear_reward_functions.append(rf_handle)

    w, log_mean_error = train_linear_model(non_linear_reward_functions[0], env=env_config)

    print('w: {}'.format(w))
    print('first: {}'.format(log_mean_error[0]))
    print('last: {}'.format(log_mean_error[-1]))

    return log_mean_error


def eval_train_linear_model_on_linear_reward_function():

    n_tasks = 1

    env_config = eu.AttrDict(
        cls = rlexp.env.project.continuous_sf_01.RBFStateTwoDTrackRacerEnv,
        start_state=[
            [0.1, 0.9],  # x
            [0.1, 0.9],  # y
            [-np.pi, np.pi]  # angle
        ],
        stochasticity = (0.005, 0.005, 0.005),
        markers=np.array([
            [0.25, 0.75],
            [0.75, 0.25],
            [0.75, 0.6],
        ]),
        wall_proximity_feature = True,
    )

    # set seed so that the sampling of the tasks is deterministic
    eu.misc.seed(1)


    # sample weights between [-1, 1]
    reward_weights_per_task = np.random.rand(n_tasks, env_config.markers.shape[0] + env_config.wall_proximity_feature) * 2 - 1

    reward_functions = []
    for task_idx in range(n_tasks):
        reward_weights = reward_weights_per_task[task_idx, :]
        reward_func = lambda feature, w=reward_weights: np.sum(w * np.array(feature))
        reward_func_descr = dict(reward_weight_vector=reward_weights)
        reward_functions.append((reward_func, reward_func_descr))

    w, log_mean_error = train_linear_model(reward_functions[0][0], env=env_config)

    print('w: {}'.format(w))
    print('first: {}'.format(log_mean_error[0]))
    print('last: {}'.format(log_mean_error[-1]))

    return log_mean_error


def search_best_learn_rate_for_non_linear_reward_functions(config=None, **kwargs):

    default_config = eu.AttrDict(
        n_reward_functions = 50,
        n_iterations = 10000,
        learn_rates = [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05],
        possible_object_features = np.array([
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
        ]),
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    eu.misc.seed(2423)

    # generate reward functions

    n_tasks = config.n_reward_functions
    possible_object_features = config.possible_object_features

    # random generator of weights for each feature
    n_features = possible_object_features.shape[0]
    feature_combination_rewards = np.random.rand(n_tasks, n_features) * 2.0 - 1.0

    non_linear_reward_functions = []
    for task_idx in range(n_tasks):
        obj_rewards = feature_combination_rewards[task_idx, :]
        r_func = lambda feature, obj_features=possible_object_features, obj_rewards=obj_rewards: non_linear_reward_func(feature, obj_features,
                                                                                                                        obj_rewards)
        non_linear_reward_functions.append(r_func)

    final_error_per_lr_and_rfunc = np.full((len(config.learn_rates), config.n_reward_functions), np.nan)

    # train weights for each
    for r_func_idx in range(config.n_reward_functions):

        for lr_idx, lr in enumerate(config.learn_rates):

            _, log_mean_error = train_linear_model(
                non_linear_reward_functions[r_func_idx],
                n_iterations=config.n_iterations,
                learn_rate=lr
            )

            final_error_per_lr_and_rfunc[lr_idx, r_func_idx] = log_mean_error[-1]

    # find best learn rate
    max_lr_idx = np.argmin(np.mean(final_error_per_lr_and_rfunc, axis=1))

    return config.learn_rates[max_lr_idx], final_error_per_lr_and_rfunc



if __name__ == '__main__':
    print('LINEAR REWARD FUNCTION')
    log_mean_error = eval_train_linear_model_on_linear_reward_function()
    plt.ylabel('log_mean_error')
    plt.show()

    print('\nNON-LINEAR REWARD FUNCTION')
    log_mean_error = eval_train_linear_model_on_non_linear_reward_function()
    plt.plot(log_mean_error)
    plt.ylabel('log_mean_error')
    plt.show()

    # # identify best learn rate
    # learn_rates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0]
    # best_lr, final_error_per_lr_and_rfunc = search_best_learn_rate_for_non_linear_reward_functions(
    #     learn_rates = learn_rates,
    #     n_reward_functions = 50,
    #     n_iterations = 10000,
    # )
    #
    # print('Mean error (n={}) per learn rate:'.format(final_error_per_lr_and_rfunc.shape[1]))
    # for lr_idx, lr in enumerate(learn_rates):
    #     mean_error = np.mean(final_error_per_lr_and_rfunc[lr_idx,:])
    #     print('{}: {}'.format(lr, mean_error))
    #
    # print('\nBest learn rate: {}'.format(best_lr))

