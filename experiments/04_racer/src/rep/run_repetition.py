#!/usr/bin/env python
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import rlexp as rl
import exputils as eu
import numpy as np
from repetition_config import config
from train_linear_model import train_linear_model
import exputils.data.logging as log


def log_transitions(log, env, agent, step, episode, step_per_episode, transition, exp_info):

    # only record transitions from the QL agents to be used to learn the features
    if isinstance(agent, rl.agent.project.xi_learning_01.DNNQLearning):

        # only record the first 10 tasks (i.e. phases)
        if exp_info.phase < 10:

            # only record every 10th transition, to reduce the amount of data
            if exp_info.step % 10 == 0:
                log.add_value('transitions', transition)
                log.add_value('phase_per_transition', exp_info.phase)


# train linear weights in case the DNNSFQLearning agent needs them
if hasattr(config.agent, 'is_set_initial_reward_weights') and config.agent.is_set_initial_reward_weights:

    # set seed from config
    eu.misc.seed(config)

    for rfunc_idx, (rfunc, rfunc_descr) in enumerate(config.reward_functions):

        w, log_mean_error = train_linear_model(
            rfunc,
            config.env,
            n_iterations=10000,
            batch_size=50,
            learn_rate=1.0
        )
        rfunc_descr['reward_weight_vector'] = w

        config.reward_functions[rfunc_idx] = (rfunc, rfunc_descr)

        # log the error from the training of w
        for err in log_mean_error:
            log.add_value('linear_model_error_per_iteration_for_task_{}'.format(rfunc_idx), err)

    log.save()


log, agent, env = rl.exp.continual_rf_transfer.run_training(
    resume_from_log=True,
    save_log_automatically=True,
    config=config,
    log_functions=log_transitions
)
