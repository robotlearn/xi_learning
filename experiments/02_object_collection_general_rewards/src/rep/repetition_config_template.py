##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import rlexp
import rlexp.agent.project.xi_learning_01 as agents
import rlexp.utils as utils
import gym
import torch

config = eu.AttrDict(
    seed = 4568745 + <repetition_id>,

    agent = eu.AttrDict(
        cls=agents.<agent>,
        features = eu.AttrDict(
            cls=agents.<feature_type>
        )
    ),

    env = eu.AttrDict(
        cls=rlexp.env.project.xi_learning_01.Barreto2018TwoDObjectCollection,

        objects = np.array([  # each object has a [x_pos, y_pos, radius]
            [0.08, 0.56, 0.04],
            [0.08, 0.92, 0.04],
            [0.20, 0.50, 0.04],
            [0.44, 0.56, 0.04],
            [0.44, 0.92, 0.04],
            [0.50, 0.20, 0.04],
            [0.50, 0.80, 0.04],
            [0.56, 0.08, 0.04],
            [0.56, 0.44, 0.04],
            [0.80, 0.50, 0.04],
            [0.92, 0.08, 0.04],
            [0.92, 0.44, 0.04],
        ]),

        # circle - white    [1, 0, 1, 0]
        # circle - black    [1, 0, 0, 1]
        # box - white       [0, 1, 1, 0]
        # box - black       [0, 1, 0, 1]
        object_features = np.array([
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
            [0, 1, 0, 1],
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [1, 0, 1, 0],
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
            [0, 1, 1, 0],
        ])
    ),

    n_tasks = <n_tasks>,
    n_max_steps_per_phase = 20000,
)

################
# agent configuration depends on the used agent (config.agent_class)

# set general agent config
config.agent = eu.combine_dicts(config.agent, dict(<agent_config>), config.agent.cls.default_config())

if config.agent.cls == agents.QLearningNumpy:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <q_lr>

if config.agent.cls == agents.Barreto2018SFQL:
    config.agent.epsilon = <epsilon>
    config.agent.alpha_w = <r_lr>
    config.agent.alpha = <psi_lr>

if config.agent.cls == agents.Barreto2018SFQLFeature:
    config.agent.epsilon = <epsilon>
    config.agent.alpha_w = <r_lr>
    config.agent.alpha = <psi_lr>

if config.agent.cls == agents.DiscreteMFXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

if config.agent.cls == agents.DiscreteMBXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.onestep_sf_model = eu.combine_dicts(dict(<sf_model_config>), config.agent.onestep_sf_model)
    config.agent.onestep_sf_model.optimizer.lr = <sf_model_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

if config.agent.cls == agents.OneDDiscretizedContinuousPhiMFXiFeatureNumpy:
    config.agent.xi_func = eu.combine_dicts(dict(<xi_config>), config.agent.xi_func)
    config.agent.xi_func.alpha = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.alpha_w = <r_lr>

# provide the path to the feature model if one should be used
# this depends on the number of features <n_feature_dims> which defines experiment id used for computing the features
if '<feature_type>' == 'ModeledFeatures':
    # the learned features have experiment ids with 4xx and 8xx where x is the same repetition as the current one
    src_feature_exp_id = <n_feature_dims> * 100 + <repetition_id>
    config.agent.features.feature_model_path = '../../../learn_features/experiments/{}/{}/data/final_model.dill'.format(
        eu.EXPERIMENT_DIRECTORY_TEMPLATE.format(src_feature_exp_id),
        eu.REPETITION_DIRECTORY_TEMPLATE.format(0), # always use repetition 0 from learned features
    )


##############################################################
# sample random tasks, i.e. random non-linear reward functions

def reward_func(feature, object_features, reward_per_object_feature):
    # make sure features are numpy arrays
    feature = np.array(feature)

    # check if one of the features in the description is the same as the current feature
    is_match = np.apply_along_axis(
        np.all,
        1,
        feature[:-1] == object_features)

    # if yes, use its reward
    if np.any(is_match):
        reward = reward_per_object_feature[is_match][0]
    else:
        reward = 0.0

    # give a reward of 1 for reaching the goal state
    reward += feature[-1]

    return reward

possible_object_features = np.array([
    [1, 0, 1, 0],
    [1, 0, 0, 1],
    [0, 1, 1, 0],
    [0, 1, 0, 1],
])

# set seed so that the sampling of the tasks is deterministic
eu.misc.seed(config)

# random generator of weights for each feature
n_features = possible_object_features.shape[0]
feature_combination_rewards = np.random.rand(config.n_tasks, n_features) * 2.0 - 1.0

non_linear_reward_functions = []
for task_idx in range(config.n_tasks):
    obj_rewards = feature_combination_rewards[task_idx, :]
    r_func = lambda feature, obj_features=possible_object_features, obj_rewards=obj_rewards: reward_func(feature, obj_features, obj_rewards)
    non_linear_reward_functions.append(r_func)

config.reward_functions = non_linear_reward_functions

# remove the first 20 tasks if learned features are used
# as these tasks were used to collect the data to learn the features
if '<feature_type>' in ['ModeledFeatures', 'LinearFeatures']:
    config.reward_functions = config.reward_functions[20:]